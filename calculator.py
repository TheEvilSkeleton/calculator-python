import math

# Set variables
affirmation = [ "y", "yes", "ye", "1", "" ]
restart = ""
#ask
#total

# Ask if the user wants to start
restart = str(input("Do you want to start the calculator?\n[Y/n] "))

# Confirm if the user wants to start or not
while restart.lower() in affirmation:

    # Ask for initial number; repeat if invalid
    while True:
        try:
            total = float(input("\nWhat number do you want to start with?\n>> "))
            break
        except ValueError:
            print("Oops! That was no valid number. Try again...")

    # Ask for operation
    ask = ""
    while ask.lower() in affirmation:
        ask = str(input("\nWhat operation do you want to use?\n0. Exit\n1. Addition\n2. Subtraction\n3. Multiplication\n4. Division\n\nChoose the operation you want to use:\n>> "))

        # Exit if input is 0 or "exit" (case insensitive)
        if ask == "0" or ask.lower() == "exit":
            print("\nExiting...")
            exit()

        # Addition is input is 1 or "addition" (case insensitive)
        elif ask == "1" or ask.lower() == "addition":
            # Ask for number; repeat if invalid
            while True:
                try:
                    addend = float(input("\nHow much do you want to add?\n>> "))
                    break
                except ValueError:
                    print("Oops! That was no valid number. Try again...")
            # Print total and calculate
            print("\n" + str(total), "+", str(addend))
            total = total + addend

        # Subtraction if input is 2 or "subtraction" (case insensitive)
        elif ask == "2" or ask.lower() == "subtraction":
            # Ask for number; repeat if invalid
            while True:
                try:
                    subtrahend = float(input("\nHow much do you want to subtract?\n>> "))
                    break
                except ValueError:
                    print("Oops! That was no valid number. Try again...")
            print("\n" + str(total), "-", str(subtrahend))
            total = total - subtrahend

        # Multiplication if input is 3 or "multiplication" (case insensitive)
        elif ask == "3" or ask.lower() == "multiplication":
            # Ask for number; repeat if invalid
            while True:
                try:
                    factor = float(input("\nHow many times do you want to multiply?\n>> "))
                    break
                except ValueError:
                    print("Oops! That was no valid number. Try again...")
            print("\n" + str(total), "*", str(factor))
            total = total * factor

        # Division if input is 4 or "division" (case insensitive)
        elif ask == "4" or ask.lower() == "division":
            # Ask for number; repeat if invalid
            while True:
                try:
                    divisor = float(input("\nWhat do you want to divide by?\n>> "))
                    break
                except ValueError:
                    print("Oops! That was no valid number. Try again...")
            print("\n" + str(total), "/", str(divisor))
            total = total / divisor
        # Repeat if invalid
        else:
            print("\nOops! That was no valid number. Try again...")
            ask = ""
            continue

        # Output total
        print("Total:", total)

        # Set "ask" to nothing
        ask = ""

        # Ask if continue
        if ask.lower() in affirmation:
            ask = str(input("\nDo you want to continue?\n[Y/n] "))
        # Else do nothing
        else:
            pass

        # Ask if restart
        if ask.lower() not in affirmation:
            restart = str(input("\nDo you want to restart?\n[Y/n] "))
        # Else do nothing
        else:
            pass

# Print exiting, and exit
print("\nExiting...")
exit()
